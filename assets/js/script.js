  let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

 let foundUsername = registeredUsernames.find(function (username){
 	console.log(username);
 	return username === 'superPhoenix';
 })

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michealKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com'
];

function email(a){
	let exist = registeredEmails.includes(a);
	if (exist == true){
		alert("Email Already Exist");
	}
	else {
		alert("Email is available, proceed to registration");
	}
}

email('pedro_himself@yahoo.com');



// ---------------------------------------------------------------
 /*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
// */
// 		let carBrands = [
// 			'BMW',
// 			'Dodge',
// 			'Maserati',
// 			'Porsche',
// 			'Chevrolet',
// 			'Ferrari',
// 			'GMC',
// 			'Porsche',
// 			'Mitsubhisi',
// 			'Toyota',
// 			'Volkswagen',
// 			'BMW'
// 		];

// function firstTime(brand){
// 	console.log(carBrands.indexOf(brand));
// }

// function lastTime(brand){
// 	console.log(carBrands.lastIndexOf(brand));
// }

// firstTime('Porsche');
// lastTime('Porsche');
// -----------------------------------------------------------------------------------

  			
			// Mini-Activity

			// Given a set of characters, 
			// 	-form the name, "Martin" as a single string.
			// 	-form the name, "Miguel" as a single string.
			// Use the methods we discussed so far.

			// save "Martin" and "Miguel" to variables:
			// 	-name 1 and name 2

			// log both variables on the console.



			// Given a set of characters, 
			// 	-form the name, "Martin" as a single string.
			// 	-form the name, "Miguel" as a single string.
			// Use the methods we discussed so far.

			// save "Martin" and "Miguel" to variables:
			// 	-name 1 and name 2

			// log both variables on the console.


// let name1= "Miguel"
// let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];
// let splitedName = Array.from(name1);
// let result = [];

// console.log(splitedName);
// Output: (6)['M', 'i', 'g', 'u', 'e', 'l']

// for(let a=0; a < charArr.length-1;a++ ){
// 	for (let b=0; b < splitedName.length-1; b++){
// 		if (charArr[a] == splitedName[b]){
			
// 			console.log(charArr[a] + " "+ splitedName[b]+' True');
// 			console.log(result.unshift(charArr[a].toString()));
// 				// break;
// 			}
// 		else {
// 			// console.log('False');
// 		}
// 	}
// }

// // let name1 = charArr.slice(5,11).join("");
// // let name2 = charArr.slice(13,19).join("");

 
 let array = ['Europe'
 			,'Singapore'
 			,'Japan Again'
 			,'US'
 			,'Canada'
 			,'Thailand'
 			,'Korea'];

 for(let a=0;a<array.length;a++){
 	console.log(array[a]);
 }


 array[0] = 'Tondo,Manila';
 array[array.length-1]='National Teachers College';

 console.log(array);

 // array.push('Tondo,Manila'); -- add at the end of the array
 // array.unshift('National Teachers College'); - add at the start of the array
 // array.pop - removes last element at the end of the array.

 	// .pop - can also return the removed item.

 //array.shift - removes the first element of the array.


 array1.shift();
 console.log(array1);

 array1.pop();
 console.log(array1);;

 array.push('Michael');
 console.log(array1)

 array.unshift('George');
 console.log(array1);



let videoGame = ['PS4'
				,'PS5'
				,'Switch'
				,'Xbox'
				,'Xbox1'];

let microsoft = videoGame.slice(3,6);
console.log(microsoft);

let nintendo = videoGame.slice(0,3);
console.log(nintendo);
//----------------------------------------------------------------//

Three ways to Add JavaScript
	- External
		- 
	- Internal
	 	- JavaScript is embedding the code directly inside script tags inside the head element.
	- Inline
		- embed JavaScript inside the html elements, like handling events

Writing Comments in JavaScript:
There are two ways of writing comments in JS:
	// - single line comments

	/*
		multi line comment
	 */

JavaScript
	- we can see or log message in our console.
		- Console 
			- are part of our browsers which will allow us to see/log message, data or information from our programming language -- JavaScript.

			For most browsers, console can be accessed through its developer tools in the console tab.

			In fact, consoles in browser allow us to add some JavaScript expressions.

Statements
	- statements are instructions, expressions we add to our programming language which will then be communication to our computers.

		- statements in JavaScript commonly ends in semicolon(;). However, JavaScript has implemented way of automatically adding semicolon at the end of our statemetns. Which, therefore, means that, unlike other languages, JS does NOT required semicolon.

			- semicolons in JS are mostly used to mark the end of the statement.

Syntax
	- syntax in programming, is a set of a rules that describes how statements are properly made/constructed.

Lines/blocks of code must follow a certain set of rules for it to work. Because, remember, you are not merely communicating with another human, in fact you are communicating with a computer.

Variables
	In HTML, elements are container of other elements and text.

	In JS, variable are containers of data. A give name is used to describe a piece of data

	Variables also allow us to user or refer to data multiple times. 
ex.
	num - is our variable
	10 - being the value/data

variable keyword
	let
	const

/*Creating Variables*/

To create a variable, there are 2 ways to be done:
	- Declaration which actually allows to create the variable
	- Initialization which allows to add an initial value to a variable

Ex. Declaration
	let num;
	const pi;

Ex. Initialization
	let num = 10;
	const pi = 3.1415	


let myVariable;
	- we can create variable without an initial value. However, when logged into the console, the variable will return a value of undefined.

	Undefined is a data type that indicates that variable does exist however there was no initial value.

You can always initialize a variable after declaration by assigning value to the variable with the use of the assignment operator (=)

myVariable = 'New Initialized Value';

You cannot and should not access a variable before it's been created or declared.

Can you use or refer to a variable that has not been declared or created?
NO.This will result in an error: NOT DEFINED

Undefined vs Not Defined
Undefined means a variable has been declared but there's no initial value
	- undefined is data type

Not Defined means that the variable you are tyring to refer or access does NOT exist
	- NOT DEFINED is an error

Note: Some erros in JS, will stop the program from further executing running

Let vs const

Constants
	- in JS are basically variables except that their values are meant to be unchanging after being initialized.
	- instead of var, const keyword is used

Let
	- in JS is variable that their values are meant to change after being initialized.

Can you declare a const variable without initialization?
 - No. An error will occur.

Can you re-assign another value to a const variable?
 - No. You cannot and should not re-assign a values to a const variable.

Guides on Variable Names:
1. When naming variables, it is important to create variables that is descriptive and indicative of the data it contains.
	ex:
		let firstName = 'Juan'; - good variable name
		let pokemon = 2500; - bad variable name
2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. 
	- because there are several keywords in JS that starts in capital letters.

	ex:
		let FirstName = 'Juan'; - bad variable name
		let firstName = 'Juan'; - good variable name
3. Do not add spaces to your variable names. Use camelCase for multiple words or underscores.

	ex:
		let first name = 'Juan' - bad variable name

		 - camelCase is when we have the first word in small caps or lowercase and add the next word added without space but the first letter of the word is capitalize

		 - readability

		 let firstName = 'Juan';
		 let first_name = 'John';

Data types
	To create data with particular data types, some data types require adding with literal.
		- string literals = '', "" and recently: ``(template literals) - backtick
		- object literals = {}
		- array literals = []

String
	- strings are a series of alphanumeric characters that create a word, or phrase, a name or anything related to creating text.
	- string literals such as ''(single quote) or "" (double quote) are used to write/create strings.

		ex:
			let country = 'Philippines';
			let province = "Manila";

Mini-Activity
	Create 2 variables named firstName and lastName
	Add you first name and last as strings to its appropriate variables
	Log your firstName and lastName variable in the console at the same time.



let country = 'Philippines';
let province = "Manila";

console.log(typeof(country));
console.log(province);

let firstName = 'Thonie'
let lastName = 'Fernandez'
let fullName = firstName + " " + lastName
console.log(fullName);

let word1 = 'is';
let word2 = 'bootcamper';
let word3 = 'of';
let word4 = 'School';
let word5 = 'Zuitt Coding';
let word6 = 'a';
let word7 = 'Institute';
let word8 = 'Bootcamp'
let space = ' ';
/*
	Mini-Activity
	Create a variable called sentence.
	Combine the variables to form a single string which would legibly and understandably create a sentence that would say that you are a bootcamper of Zuitt.
	Log the sentence variable in the console.

 */
let sentence = firstName + space + lastName + space + word1 + space + word6 + space + word2 + space + word3 + space + word5 + space + word8 + space + word7;

console.log(sentence);
 // Template Literals (``) allows us to create a string with the use of backticks.
 // ${placeholder}

 let sentenceLiteral = `${fullName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word8} ${word7}`;
 console.log(sentenceLiteral);

 //number data type

let numString1 = '7';
let numString2 = '6';
let numA = 7;
let numB = 5;
let num3 = 5.5;
let num4 = .5

console.log(numString1 + numString2);
console.log(numA + numB);
console.log(numA + num3);
console.log(num3 + num4);

// forced coercion - When one data type is forced to change to complete an operation
// string + num = concatenation
console.log(numString1 + numA);//'77' resulted in concatenation
console.log(num3 + numString2);//'5.56'

//parseInt() - this can change the type of numeric string to a proper number
console.log(num4 + parseInt(numString1));// numString1 was parsed into a proper number

let sum = numA + parseInt(numString1);
console.log(sum); //numString1 was parsed into a proper number

// Mathematical Operations (-, *, /, %)
// subtraction
	console.log(num3 - numA);//-1.5 results in proper mathematical operation
	console.log(num3 - num4);
	console.log(numString1 - numB);//2 results in proper mathematical operation because the string was forced to become a number
	console.log(numString2 - numB);
	console.log(numString1 - numString2);//In subtraction, numeric string will not concatenate and instead will be forcibly changed its type and subtract in proper mathematical operation

	let sample = 'thonie';
	console.log(sample - numString1);// NaN - result in not number. When trying to perform subtraction between alphanumeric string and numeric string. The result is NaN

// Multiplication
	console.log(numA * numB);
	console.log(numString1 * numA);
	console.log(numString1 * numString2);

	let product = numA * numB;
	let product2 = numString1 * numA;
	let product3 = numString1 * numString2;

//Division
	console.log(product / numB);//7
	console.log(product2 / 5);//9.8
	console.log(numString2 / numString1);

//Division/Multiplication by 0
	console.log(product * 0);
	console.log(product3 / 0);
	//division by 0 is not accurately and should not be done - INFINITY

//%Modulo - remainder of division operation
	console.log(product2 % numB);//product2/numB = remainder - 4
	console.log(product3);
	console.log(product2);
	sam = product3%7;
	console.log(sam)
	console.log(product2%product3);
//Boolean (true or false)
//Boolean is usually used for logic operations or if-else condition

// When creating a varible which will contain boolean, the variable name is usually a yes or no question

	let isAdmin = true;
	let isMarried = false;
	let isMVP = true;
	let isGwapo = true;

	//you can contatenate string + boolean
	console.log('Does Rommel is handsome?' + isGwapo);
	console.log(`Is he is handsome?` +isGwapo);

//Arrays
let array = ['Goku', 'Piccolo', 'Gohan', 'Vegeta'];
console.log(array);
let array1 = ['One Punch Man', true, 500, 'Saitama'];
console.log(array1)

// Objects
let hero = {
	heroName: 'One Punch Man',
	isActive: true,
	salary: 500,
	realName: 'Saitama',
	height: 200
}
console.log(hero)

/*Mini-Activity
	Create a variable with a group of data
		- The group of data should contain names from you favorite band.

	Create a variable which can contain multiple values of differing types and describe a single person.
		- this data type should be able to contain multiple key value pairs:
			firstName: <value>
			lastName: <value>
			isDeveloper: <value>
			hasPortfolio: <value>
			age: <value>


*/

let bandMembers = ['Ringo', 'Paul', 'John', 'George'];
let person = {
			firstName: 'Thonie',
			lastName: 'Fernandez',
			isDeveloper: 'true',
			hasPortfolio: 'true',
			age: 18
};
console.log(bandMembers, person);
let grades = {
	quiz1: 90,
	quiz2: 95,
	quiz: 100
};

grades = [87, 88, 90];

//Undefined vs Null

let sampleNull = null;

let sampelUndefined;
console.log(sampleNull);
console.log(sampelUndefined);

let foundResult = null;
let person2 = {
	name: 'Juan',
	isAdmin: true,
	age: 35
}
console.log(person2.isAdmin);

// function
function greet() {
		console.log('Hello, my name is Saitama.');
	}
// invoke a function (calling a function) - functionname
greet();
greet();
greet();
greet();
greet();

// Paramater and Arguments
// A parameter acts a name variable/container that exists ONLY inside of the function. This is used as to store information to act as a stand-in or the container the value passed into the funnction as argument
function printName(name){
	console.log(`My name is ${name}`)
}
printName('thonie');
// when function is invoked and data is passed, we call the data as argument.
//In this invocation, 'thonie' is an argument passed into our printName function and is represented by the "name" parameter within our function
function displayNum(number){
	console.log(number);
};
displayNum(5000);

// Multiple Parameters and Arguments

function displayFullName(firstName, lastName, age){
	console.log(`${firstName}, ${lastName} ${age}`);
}
displayFullName('Juan', 'Masipag', 29);

// return keyword
function createName(firstName, lastName){
	return `${firstName} ${lastName}`
	console.log("I will no longer run because the function's value/result has been return")
}

let fullName1 = createName('Tom', 'Cruise');
let fullName2 = createName('Brad', 'Pitt');
console.log(fullName1)
console.log(fullName2)

===========
Notes:
Three ways to Add JavaScript
	- External
		- 
	- Internal
	 	- JavaScript is embedding the code directly inside script tags inside the head element.
	- Inline
		- embed JavaScript inside the html elements, like handling events

Writing Comments in JavaScript:
There are two ways of writing comments in JS:
	// - single line comments

	/*
		multi line comment
	 */

JavaScript
	- we can see or log message in our console.
		- Console 
			- are part of our browsers which will allow us to see/log message, data or information from our programming language -- JavaScript.

			For most browsers, console can be accessed through its developer tools in the console tab.

			In fact, consoles in browser allow us to add some JavaScript expressions.

Statements
	- statements are instructions, expressions we add to our programming language which will then be communication to our computers.

		- statements in JavaScript commonly ends in semicolon(;). However, JavaScript has implemented way of automatically adding semicolon at the end of our statemetns. Which, therefore, means that, unlike other languages, JS does NOT required semicolon.

			- semicolons in JS are mostly used to mark the end of the statement.

Syntax
	- syntax in programming, is a set of a rules that describes how statements are properly made/constructed.

Lines/blocks of code must follow a certain set of rules for it to work. Because, remember, you are not merely communicating with another human, in fact you are communicating with a computer.

Variables
	In HTML, elements are container of other elements and text.

	In JS, variable are containers of data. A give name is used to describe a piece of data

	Variables also allow us to user or refer to data multiple times. 
ex.
	num - is our variable
	10 - being the value/data

variable keyword
	let
	const

/*Creating Variables*/

To create a variable, there are 2 ways to be done:
	- Declaration which actually allows to create the variable
	- Initialization which allows to add an initial value to a variable

Ex. Declaration
	let num;
	const pi;

Ex. Initialization
	let num = 10;
	const pi = 3.1415	


let myVariable;
	- we can create variable without an initial value. However, when logged into the console, the variable will return a value of undefined.

	Undefined is a data type that indicates that variable does exist however there was no initial value.

You can always initialize a variable after declaration by assigning value to the variable with the use of the assignment operator (=)

myVariable = 'New Initialized Value';

You cannot and should not access a variable before it's been created or declared.

Can you use or refer to a variable that has not been declared or created?
NO.This will result in an error: NOT DEFINED

Undefined vs Not Defined
Undefined means a variable has been declared but there's no initial value
	- undefined is data type

Not Defined means that the variable you are tyring to refer or access does NOT exist
	- NOT DEFINED is an error

Note: Some erros in JS, will stop the program from further executing running

Let vs const

Constants
	- in JS are basically variables except that their values are meant to be unchanging after being initialized.
	- instead of var, const keyword is used

Let
	- in JS is variable that their values are meant to change after being initialized.

Can you declare a const variable without initialization?
 - No. An error will occur.

Can you re-assign another value to a const variable?
 - No. You cannot and should not re-assign a values to a const variable.

Guides on Variable Names:
1. When naming variables, it is important to create variables that is descriptive and indicative of the data it contains.
	ex:
		let firstName = 'Juan'; - good variable name
		let pokemon = 2500; - bad variable name
2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. 
	- because there are several keywords in JS that starts in capital letters.

	ex:
		let FirstName = 'Juan'; - bad variable name
		let firstName = 'Juan'; - good variable name
3. Do not add spaces to your variable names. Use camelCase for multiple words or underscores.

	ex:
		let first name = 'Juan' - bad variable name

		 - camelCase is when we have the first word in small caps or lowercase and add the next word added without space but the first letter of the word is capitalize

		 - readability

		 let firstName = 'Juan';
		 let first_name = 'John';

Data types
	To create data with particular data types, some data types require adding with literal.
		- string literals = '', "" and recently: ``(template literals) - backtick
		- object literals = {}
		- array literals = []

String
	- strings are a series of alphanumeric characters that create a word, or phrase, a name or anything related to creating text.
	- string literals such as ''(single quote) or "" (double quote) are used to write/create strings.

		ex:
			let country = 'Philippines';
			let province = "Manila";


	Solution of Mini-Activity
		let firstName = 'Thonie',
		let lastName = 'Fernandez'

		console.log(firstName, lastName); - to view 2 variables

	Operations:
		- Concatenation
			- is a process/operation wherein we combine two strings as one.
			- using the plus(+)
				- JS string - spaces are also counted as characters
			console.log(firstName + " " + lastName)
Template Literals
	- allows is to create string with the use of backticks
	- also allows to easily concatenate strings withou the use of (+)
	- also allows to ember or add variables and even expression in our string with the use of placeholder ${}.

Number (Data type)
	- it can be used for mathematical operation
		- integers (whole numbers)
		- floats (decimal numbers)
let numString1 = '7';
let numString2 = '5';
let num1 = 8;
let num2 = 6

Arrays
	- are special kind of data type used to store multiple values.
	- Arrays can actually store data with different data typed BUT the best practice, arrays are used to contain multiple values of the SAME data type.
	- values in array are separated by commas
	- an array is created with an array literal []

let array = ['Goku', 'Piccolo', 'Gohan', 'Vegeta']

Objects
	- objects are another special kind of data type used to mimic reak world objects
	- used to create comples data the contain pieces of information that are relevant to each other
	- an object is created with an object literal {}
	- each data/value are paired with a key
	- each field is called a property
	- each field is separated by a comma
let hero = {
	heroName: 'One Punch Man',
	isActive: true,
	salary: 500,
	realName: 'Saitama',
	height: 200
}

Null
	- is explicit absence of data/value. 
	This is done to project that a varibake contains nothing over undefined.

Undefined
	- it means that there is no data in the variable BECAUSE the variable has not been assigned an initial value.

Functions
	- functions in JS, are lines/block of codes that tell our application to perform a certain task when called/invoked.

	- function keyword + name of function + ()

	function greet() {
		console.log('Hello, my name is Saitama.');
	}

Multiple Parameters and Arguments
 - A function can not only receive single argument but also multiple arguments as long as it matches the number of parameters

Instructions:

	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.

	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console.

Pushing Instructions:

Go to Gitlab:
	-in your zuitt-projects folder and access b131 folder.
	-inside your b131 folder create a new folder/subgroup: s14

Go to Gitbash:
	-go to your b131/s14 folder and access activity folder - a1
	-initialize activity folder as a local repo: git init
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes javascript intro activity"
	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your s14/activity repo (URL on browser not the URL from clone button) and link it to boodle:

	WD078-14 | Javascript Introduction


s-14 Activity
Instructions:

	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.

	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console.

Pushing Instructions:

Go to Gitlab:
	-in your zuitt-projects folder and access b131 folder.
	-inside your b131 folder create a new folder/subgroup: s14

Go to Gitbash:
	-go to your b131/s14 folder and access activity folder
	-initialize activity folder as a local repo: git init
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes javascript intro activity"
    -connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-push your updates to your online repo: git push origin master

Go to Boodle:
	-copy the url of the home page for your s14/activity repo (URL on browser not the URL from clone button) and link it to boodle:

	WD078-14 | Javascript Introduction

=============================================================================================================================================
/*answer activity*/
let favoriteFood = "Cordon Blue";
let sum = 150 + 9;
let product =  100 * 90;
let isActive = true;
let favoriteResturant = ["Marugame Udon", "Blackbird", "Balay Dako"];
let favoriteActor ={
	firstName: "Joon-ki",
	lastName:  "Song",
	stageName: "Song Joon-Ki",
	birthDay: "September 25 1985",
	age: 36,
	bestTVShow:	"Descendants of the Sun",
	bestMovie: "Battleship Island",
	isActive: true
}

console.log(favoriteFood);
console.log(sum);
console.log(product);
console.log(isActive);
console.log(favoriteResturant);
console.log(favoriteActor);

/*function*/
function divideNum(num1, num2){
	// console.log(num1/num2);
	return num1/num2;
};

let quotient = divideNum(50, 10);

console.log(`The result of the division is: ${quotient}`);

// Mathematical operators
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

// num1 = num1 +  num4;
num1 += num4;
console.log(num1);

// num2 = num2 + num4;
num2 += num4;
console.log(num2);

// num1 = num1 * 2;
num1 *= 2;
console.log(num1);

let string1 = "Boston ";
let string2 = " Celtics";

// string1 = string1 + string2;
string1 += string2;
console.log(string1);

// num1 = num1 - string1;
num1 -= string1;
console.log(num1);

let string3 = "Hello everyone";
let myArray = string3.split("", 3);
console.log(myArray);

// Mathematical operations - follows MDAS.

let mdasResult = 1 + 2 - 3 * 4 / 5;
/*
	3*4 = 12
	12/5 = 2.4
	1+2 =3
	3-2.4 = 0.6

 */
console.log(mdasResult);
// PEMDAS - Parenthesis, exponents, multiplication, division, addition and subtraction
let pemdasResult = 1 + (2-3) * (4/5);
/*
	4/5 = .8
	2-3 = -1
	-1*.8 = -0.8
	1+ -0.8 = .0
*/
console.log(pemdasResult);

// Increment and Decrement
// Two types Increment: Pre-fix and Post-fix

let z = 1;
//Pre-fix Incrementation
++z;
console.log(z);

// Post-fix Incrementation
z++;
console.log(z);
console.log(z++);
console.log(z);

// Pre-fix vs Post-fix Incrementation
console.log(z++);
console.log(z);

console.log(++z);

let n = 1;
console.log(++n);// 1 + n = 2
console.log(n);

console.log(n++);// n + 1 = 3
console.log(n);


// Pre-fix and Post-fix Decrementation
console.log(z);
console.log(z--);
console.log(z);

// comparison Operators - used to compare values
// Equality or Loose Equality Operator (==)
console.log(1 == 1);//true
console.log('1' == 1);

// strict equality
console.log(1 === 1);
console.log('1' === 1);

console.log('apple' == 'apple');
let isSame = 55 == 55;
console.log(isSame);

console.log(0 == false); // force coercion
console.log(1 == true);
console.log(true == 'true'); //1 != NaN

console.log(true == '1');
console.log('0' == false);

// Strict equality - checks both value and type
console.log(1 === '1');
console.log('Juan' === 'Juan')
console.log('Maria' === 'maria')

// Inequality Operators (!=)
	// Checks whether the operands are NOT equal and/or have different value
	// will do type coercion if the operands have different types:

	console.log('1' != 1);//false
	//false > both operands are converted to numbers
	//'1' converted into number is 1
	//1 converted into number is 1
	//1 == 1
	//not inequal

	console.log('James' != 'John')

	console.log(1 != "true");//true
	//with type conversion: true was converted to 1
	//"true" was convered into a number but results NaN
	//1 is not equal to NaN
	//it IS inequal

// strict inequality operator (!==) > it checks whether the two operand have different values and will check if they have different types

	console.log('5' !== 5)//true
	console.log(5 !== 5)//false

let name1 = 'Juan';
let name2 = 'Maria';
let name3 = 'Pedro';
let name4 = 'Perla';

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1);
console.log(numString1 === number1);
console.log(numString1 != number1);
console.log(name4 !== name3);
console.log(name1 == 'juan');
console.log(name1 === "Juan");

// Relational Comparison Operators
	// A comparison operator - check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString3 = "5500";

// Greater Than (>)
console.log( x > y);// false
console.log( w > y);

// Less Than (<)
console.log(w < y); //false
console.log(y >= y);//false
console.log(x < 1000);
console.log(numString3 < 1000);//false 
console.log(6000 < 'juan');//false

// OR Operator (|| - double pipe)
/*
	- or operator returns true if at least one of the operands are true
	T || T = T
	T || F = T
	F || T = T
	F || F = F
*/

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge1 >= requiredAge;
console.log(guildRequirement);//false

guildRequirement = isRegistered || userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement);


let guildRequirement2 = userLevel >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement2)

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin)//false

//Not operator (!)
// it turns a boolean into the opposite value: T = F F = T

let guildAdmin1 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin1);//true

let opposite1 = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite1);//true - isAdmin original value = false
console.log(opposite2);//false - isLegalAge original value = true

// if - if statement will run a code block if the condition specified is true or results to true.
const candy = 100;
if (candy >= 100){
	console.log('You got a cavity!')
}

/*
	if(true){
		block of code
	};
*/

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 30;

if(userName3.length > 10){
	console.log("Welcome to the Game online!")
};

// else statement will be run if the condition given is false of results to false
if(userName3 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining Noobies Guild");
} else {
	console.log("You too strong to be noob. :( ");
};

// else if - else if executes a statement, if the previous or the original condition is false or resulted to false but another specified condition resulted to true.
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.");
} else if(userLevel3 > 25){
	console.log("You too strong to be noob.");
} else if(userAge3 < requiredAge){
	console.log("You're too young to join the guild.");
} else {
	console.log("End of the condition,")
}

// if-else in function
function addNum(num1, num2){
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number types.");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the argumetns are not numbers.")
	};
};

addNum(5, 2)

// let customerName = prompt("Enter your name:");
// if(customerName != null) {
// 	document.getElementById("username").value = customerName;
// }

		/*
			Nested if-else if
			Mini-Activity
				add another condition to our nested if statement:
					- check if the password is at least 8 characters long
				add an else statement which will run if both conditions we not met:
					- show an alert which says "Credentials too short."

			Stretch Goals:

			add an else if statement that if the username is less than 8 characters
				- show an alert message "username is too short."
			add an else if statement that if the password is less than 8 characters
				- show an alert messge "password too short"
	
			Push it in Gitlab and paste the URL in our Boodle account s15

		 */

function login(username, password){
	if(typeof username === "string" && typeof password === "string"){
		console.log("both arguments are string.");
		if(username.length >= 8 && password.length >= 8){
			alert("Thank you for logging in")
		} else if(username.length < 8) {
			console.log("username is too short.")
		} else if(password.length < 8){
			console.log("password too short.")
		}
	} else {
		console.log("Credentials too short")
	}
};

login("Levi1234", "password");

// Switch Statement
/*
	- used an alternative to an if, else-if, else tree structure, where the data being evaluated or checked the expected output

	syntax:
	switch(expression/condition){
		case value:
			statement;
			break;
		default:
			statement;
			break;
	}
 */
let hero = "Catwoman";

switch(hero){
	case "Catwoman":
		console.log("Superhero");
		break;
	case "Jose Rizal":
		console.log("National Hero");
		break;
	case "Hercules":
		console.log("Legendary Hero");
		break;
	default:
		console.log("Invalid hero name");
		break;
}

function roleChecker(role){
	switch(role){
		case "Admin":
			console.log("Welcome Admin, to the Dashboard");
			break;
		case "User":
			console.log("You are not authorized to view this page.");
			break;
			// break terminates the code block
		case "Guest":
			console.log("Go to the registration page to register");
			break;
		default: 
			console.log("Invalid Role.");
			break;
	}
};

roleChecker("Admin");

function colorOfTheDay(day){
	if(typeof day === "string"){
		if(day.toLowerCase() === "monday"){
			alert(`Today is ${day}. Wear black.`);
		} else if(day.toLowerCase() === "tuesday"){
			alert(`Today is ${day}. Wear green.`);
		} else if(day.toLowerCase() === "wednesday"){
			alert(`Today is ${day}. Wear yellow.`);
		} else if(day.toLowerCase() === "thursday"){
			alert(`Today is ${day}. Wear red.`);
		} else if(day.toLowerCase() === "friday"){
			alert(`Today is ${day}. Wear violet.`);
		} else if(day.toLowerCase() === "saturday"){
			alert(`Today is ${day}. Wear blue.`);
		} else if(day.toLowerCase() === "sunday"){
			alert(`Today is ${day}. Wear white.`);
		} else {
			alert("Invalid input. Enter a valid day of the week.");
		}
	};
};
colorOfTheDay("June");


// switch(num){
// 	case 1 && 1:
// 		alert("You must be 13 or older to play");
// 		break;
// 	case 2:
// 		alert("You are old enough");
// 		break
// }

// function gradeEvaluator(grade){
// 	// evaluate the grade input and return the letter distinction
// 	// if the grade is less than or equal to 70 = F
// 	// if the grade is greater than or equal to 71 = C
// 	// if the grade is greater than or equal to 80 = B
// 	// if the grade is greater than or equal to 90 = A
// 	if(grade >= 90){
// 		return "A"
// 	} else if(grade >= 80){
// 		return "B"
// 	} else if(grade >= 71){
// 		return "C"
// 	} else if(grade <= 70){
// 		return "F"
// 	} else {
// 		return "Invalid"
// 	};
// };

// let letterDistinction = gradeEvaluator(95);
// console.log(letterDistinction);


function gradeEvaluator(grade){
	switch(true){
		case (grade >= 90):
			return("A");
			break;
		case (grade >= 80):
			return("B");
			break;
	};
};
let letterDistinction = gradeEvaluator(95);
console.log(letterDistinction);

======
Array
=====
// const array1 = ['eat', 'sleep'];
// console.log(array1);

// const array2 = new Array('pray', 'play');
// console.log(array2);


// empty array
// const myList = [];

// array of numbers
// const numArray = [2, 3, 4, 5];

// array of strings
// let stringArray = ['eat', 'work', 'pray', 'play'];

// array of mixed
// let newData = ['work', 1, true];

// let newData1 = [
// 	{'task1': 'exercise'},
// 	[1,2,3],
// 	function hello(){
// 		console.log('Hi I am array.')
// 	}
// ];
// console.log(newData1);

/*
	Mini-Activity

	Create an array with 7 items; all strings.
		- list seven of the places you want to visit someday
	Log the first item in the console
	Log all the items in the console.
*/
// let placesToVisit = [
// 	'Tokyo Tower', 
// 	'Great Wall of China',
// 	'Empire State Building',
// 	'Disneyland',
// 	'Hollywood',
// 	'Tower of London',
// 	'Eiffel Tower'
// ];
// console.log(placesToVisit[0]);
// console.log(placesToVisit);
// console.log(placesToVisit[placesToVisit.length-1]);
// console.log(placesToVisit.length);

// for(let i = 0; i < placesToVisit.length; i++){
// 	console.log(placesToVisit[i]);
// };

// Array manipulation
// Add element to an array - push() add element at the end of the array
// let dailyActivities = ['eat', 'work', 'pray', 'play'];
// dailyActivities.push('exercise');
// console.log(dailyActivities);
// unshift() add element at the beginning of the array
// dailyActivities.unshift('sleep');
// console.log(dailyActivities);

// dailyActivities[2] = 'sing';
// console.log(dailyActivities);

// dailyActivities[6] = 'dance';
// console.log(dailyActivities);

// Re-assign the values/items in an array:
// placesToVisit[3] = 'Giza Sphinx';
// console.log(placesToVisit);
// console.log(placesToVisit[5]);

// placesToVisit[5] = 'Turkey';
// console.log(placesToVisit);
// console.log(placesToVisit[5]);
/*
	Re-assign the values for the first and last item in the array.
		-Re-assign it with your hometown and your highschool
	log the array in the console
	Log the first and last items in the console.

 */
// placesToVisit[0] = 'San Pedro';
// placesToVisit[placesToVisit.length-1] = 'SPRCNHS';
// console.log(placesToVisit);
// console.log(placesToVisit[0]);
// console.log(placesToVisit[placesToVisit.length-1]);

// Adding items in an array without using methods:
// let array = [];
// console.log(array[0]);
// array[0] = 'Cloud Strife';
// console.log(array);
// console.log(array[1]);
// array[1] = 'Tifa Lockhart';
// console.log(array[1]);
// console.log(array);
// array[array.length-1] = 'Aerith Gainsborough';
// console.log(array);
// array[array.length] = 'Vincent Valentine';
// console.log(array);

// Array Methods
	// Manipulate array with pre-determined JS Functions
	// Mutators - these arrays methods usually change the original array

	// let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// // without method
	// array1[array.length] = 'Francisco';
	// console.log(array1);

	// .push() - allows us to add an element at the end of the array
	// array1.push('Andres');
	// console.log(array1);

	// .unshift() - allows us to add an element at the beginning of the array
	// array1.unshift('Simon');
	// console.log(array1);

	// .pop() - allows us to delete or remove the last item/element at the end of the array
	// array1.pop();
	// console.log(array1);
	// // .pop() is also able to return the item we removed.
	// console.log(array1.pop());
	// console.log(array1);

	// let removedItem = array1.pop();
	// console.log(array1);
	// console.log(removedItem);

	// .shift() return the item we removed
	// let removedItemShift = array1.shift();
	// console.log(array1);
	// console.log(removedItemShift);

/*
	Mini-Activity

		Remove the first item of array1 and log array1 in the console
		Delete the last item of array1 and log array1 in the console
		Add "George" at the start of the array1 and log array1 in the console
		Add "Michael" at the end of the array1 and loag array1 in the console

		Use array methods.
*/
// array1.shift();
// console.log(array1);
// array1.pop();
// console.log(array1);
// array1.unshift('George');
// console.log(array1);
// array1.push('Michael');

// .sort() - by default, will allow us to sort our items in ascending order.
// array1.sort();
// console.log(array1);

// let numArray = [3, 2, 1, 6, 7, 9];
// numArray.sort();
// console.log(numArray);	

// let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
// numArray2.sort((a,b)=> a-b);
// console.log(numArray2);
// .sort() converts all items into strings and then arrange the items accordinly as if they are words/text

// ascending sort per number's value
// numArray2.sort(function(a,b){
// 	return a-b
// })
// console.log(numArray2);

// descending sort
// numArray2.sort(function(a,b){
// 	return b-a
// })
// console.log(numArray2)

// let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
// arrayStr.sort(function(a,b){
// 	return b - a
// })
// console.log(arrayStr);

// .reverse() - reversed the order of the items
// arrayStr.sort();
// console.log(arrayStr);
// arrayStr.sort().reverse();
// console.log(arrayStr);


// let beatles = ['George', 'John', 'Paul', 'Ringo'];
// let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];
// splice() - allows us to remove and add elements from a given index. 
// syntax: array.splice(startingIndex, numberofItemstobeDeleted, elementstoAdd)
// lakersPlayers.splice(0, 0, 'Caruso');
// console.log(lakersPlayers);
// lakersPlayers.splice(0, 1);
// console.log(lakersPlayers);
// lakersPlayers.splice(0, 3);
// console.log(lakersPlayers);
// lakersPlayers.splice(1,1);
// console.log(lakersPlayers);
// lakersPlayers.splice(1,0, 'Gasol', 'Fisher');
// console.log(lakersPlayers);

// Non-Mutators
	// Methods that will not change the original
	// slice() - allows us to get a portion of the original array and return a new array with the items selected from the original
	// syntax: slice(startIndex, endIndex)

// let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
// computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
// console.log(computerBrands);

// let newBrands = computerBrands.slice(1, 3);
// console.log(computerBrands);
// console.log(newBrands);

let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];
console.log(fonts);

let newFontSet = fonts.slice(1, 5);
console.log(newFontSet);

let newFontSet = fonts.slice().reverse();
console.log(newFontSet);

/*
	Mini-Activity
	Given a set of data, use slice to copy the last two items in the array.
	Save the sliced portion of the array into a new variable: 
		- microsoft
	use slice to copy the third and fourth item in the array.
	Save thublime sliced portion of the array into a new variable: 
		- nintendo
	log both new arrays in the console.
*/



